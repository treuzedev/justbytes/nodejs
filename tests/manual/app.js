// // // // //
// SIMPLE TEST SERVER
// // // // //

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser')
const app = express();
const port = 5000;

const justbytes = require('../../package/justbytes.js');

app.use(cors());
app.use(bodyParser.json({'limit': '50mb'}));

app.post('/save-file', (request, response) => {
    const status = justbytes.saveMultipartFile(request.body);
    response.send(status);
});

// using nodemon for dev
// nodemon --watch ../../package app.js
app.listen(port, () => {
    console.log(`Test app listening on port ${port}`);
});
