// // // // //
// EXPORTS
// // // // //

module.exports = {
    saveMultipartFile,
};


// // // // //
// IMPORTS
// // // // //

const fs = require('fs');


// // // // //
// FUNCTIONS
// // // // //

// a function to save a multipart file
function saveMultipartFile(params) {
    
    // check if object adheres to requirements
    const incomingRequest = checkIncomingRequest(params);
    
    if (incomingRequest['status'] !== 200) {
        return incomingRequest;
    }
    
    // save current chunk
    // if there was an error, return that error
    const savingChunk = saveChunk(params);
    
    if (savingChunk['status'] !== 200) {
        return savingChunk;
    }
    
    // checks if chunk if the last one
    // if it is, join all chunks into a single file
    // places the file in specified folder or in the /tmp folder
    try {
        
        const folderContents = fs.readdirSync(savingChunk['tmpFolder']);
        const numberOfSavedChunks = folderContents.length;
        
        if (numberOfSavedChunks === params['numberOfChunks']) {
            return joinChunks(savingChunk['tmpFolder'], folderContents, params['finalDestination'], params['filename'], params['extension'],);
        }
        
    } catch (error) {
        return {
            'status': 500,
            'message': error,
        };
    }
    
    // return something everytime a chunk is saved
    return {
        'status': 200,
        'message': 'chunk saved successfully!',
        'chunk': params['chunk'],
        'filename': `${params['filename']}.${params['extension']}`,
    };
    
}


// a function to save each chunk to a tmp file
function saveChunk(params) {
    
    // create a buffer from base64 string
    const buffer = new Buffer.from(params['data'], 'base64');
    
    // create temporary file part location
    const tmpFolder = `/tmp/${params['tmpFolderName']}`;
    const tmpFilename = `${tmpFolder}/${params['chunk']}`;
    
    // check if tmpFolder exists
    // create a tmpFolder if not exists
    try {
        const folderExists = fs.existsSync(tmpFolder);
        if (!folderExists) {
            fs.mkdirSync(tmpFolder);
        }
    } catch (error) {
        return {
            'status': 500,
            'message': error,
        };
    }
    
    // save chunk to disk
    // if there is an error, return that error
    try {
        fs.writeFileSync(tmpFilename, buffer);
    } catch (error) {
        return {
            'status': 500,
            'message': error,
        };
    }
    
    // return with 200 if no errors where found
    return {
        'status': 200,
        'tmpFolder': tmpFolder,
    };
    
}


// join chunks of a file into a single file
function joinChunks(tmpFolder, chunks, finalDestination, filename, extension) {
    
    // create a temporary filename
    const tmpFilename = `/tmp/${filename}.${extension}`;
    
    // first sort the array
    chunks.sort((firstElement, secondElement) => {
        return parseInt(firstElement) - parseInt(secondElement);
    });
    
    // then loop through the array and continuously add the parts to the new file
    // creates a buffer from the file
    // appends it to the existing file, or create one if dealing with the first chunk
    for (let i = 0; i < chunks.length; i++) {
        try {
            const buffer = fs.readFileSync(`${tmpFolder}/${chunks[i]}`);
            fs.appendFileSync(tmpFilename, buffer);
        } catch (error) {
            return {
                'status': 500,
                'message': error,
            };
        }
    }
    
    // if file creation was successful
    // first delete all contents inside the folder
    // then delete the folder itself
    try {
        
        for (let i = 0; i < chunks.length; i++) {
            fs.unlinkSync(`${tmpFolder}/${chunks[i]}`);
        }
        
        fs.rmdirSync(tmpFolder);
        
    } catch (error) {
        return {
            'status': 500,
            'message': error,
        };
    }
    
    // move file from /tmp
    // take care if file has no extension
    try {
        
        if (extension) {
            finalDestination = `${finalDestination}/${filename}.${extension}`;
        } else {
            finalDestination = `${finalDestination}/${filename}`;
        }
        
        fs.renameSync(tmpFilename, finalDestination);
        
    } catch (error) {
        return {
            'status': 500,
            'message': error,
        };
    }
    
    // return filename and status if all goes well
    return {
        'status': 200,
        'message': 'file created successfully!',
        'path': finalDestination,
    };
    
}

// checks if object if of correct type
// checks if object has all required keys
// checks if there are no extra keys
// checks if keys' values are of the correct type
// returns an object indicating object validity and the the reason for an error, if any
function checkIncomingRequest(params) {
    
    if (!params) {
        return {
            'status': 500,
            'message': 'params is undefined or null..',
        };
    } else if (params.constructor !== Object) {
        return {
            'status': 500,
            'message': 'params must be an object..'
        }
    } else {
        
        const requiredKeys = [
            'chunk',
            'data',
            'extension',
            'filename',
            'finalDestination',
            'numberOfChunks',
            'tmpFolderName',
        ];
        
        const incomingKeys = Object.keys(params).sort();
        
        if (incomingKeys.length !== requiredKeys.length) {
            
            return {
                'status': 500,
                'message': 'the params object must have the exact following keys: [\'chunk\', \'data\', \'extension\', \'filename\', \'finalDestination\', \'numberOfChunks\', \'tmpFolderName\'].',
            };
            
        } else {
            
            for (let i = 0; i < incomingKeys.length; i++) {
                
                if (incomingKeys[i] !== requiredKeys[i]) {
                    return {
                        'status': 500,
                        'message': `key ${incomingKeys[i]} is not valid.`,
                    };
                }
                
            }
            
            if (!checkIfSafeInt(params['chunk'])) {
                return {
                    'status': 500,
                    'message': 'chunk needs to be a safe int..',
                };
            }
            
            if (!checkIfSafeInt(params['numberOfChunks'])) {
                return {
                    'status': 500,
                    'message': 'numberOfChunks needs to be a safe int',
                };
            }
            
            if (!checkIfString(params['data'])) {
                return {
                    'status': 500,
                    'message': 'data needs to be a string..',
                };
            } else {
                // TODO: check if base64 string is a valid base64 string
            }
            
            if (!checkIfString(params['tmpFolderName'])) {
                console.log(params['tmpFolderName'])
                return {
                    'status': 500,
                    'message': 'tmpFolderName needs to be a string..',
                };
            } else {
                
                const regex = /^[^\/].*[^\/]$/;
                const match = params['tmpFolderName'].match(regex);
                
                if (!match) {
                    return {
                        'status': 500,
                        'message': 'tmpFolderName must not start and/or end with a forward slash, for example, "justbytes/a-dir"',
                    };
                }
                
            }
            
            if (!checkIfString(params['finalDestination'])) {
                return {
                    'status': 500,
                    'message': 'finalDestination needs to be a string..',
                };
            } else {
                
                const regex = /^\/.*[^\/]$/;
                const match = params['finalDestination'].match(regex);
                
                if (!match) {
                    return {
                        'status': 500,
                        'message': 'finalDestination must start with a "/" and not end with a "/", for example, "/tmp/justbytes"',
                    };
                }
                
            }
            
            if (!checkIfString(params['filename'])) {
                return {
                    'status': 500,
                    'message': 'filename needs to be a string..',
                };
            } 
            
            if (!checkIfString(params['extension'])) {
                return {
                    'status': 500,
                    'message': 'extension needs to be a string..',
                };
            } else {
                
                const regex = /^\./;
                const match = params['extension'].match(regex);
                
                if (match) {
                    return {
                        'status': 500,
                        'message': 'extension must not start with a dot; for example, \'png\' is valid, \'.png\' is not',
                    };
                    
                }
                
            }
            
        }
        
    }
    
    return {
        'status': 200,
    };
    
}

// checks if a var is a string
function checkIfString(string) {
    if (typeof string !== 'string' || string.constructor !== String) {
        return false;
    } else {
        return true;
    }
}

// checks if a number is an int not too big
// returns false otherwise
function checkIfSafeInt(int) {
    
    if (typeof int !== 'number') {
        return false;
    } else {
        if (!Number.isSafeInteger(int)) {
            return false;
        }
    }
    
    return true;
    
}